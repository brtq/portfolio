# Portfolio



## Welcome to my portfolio

My name is Bartlomiej, I am an application developer that studied at CODERS.BAY Vienna.
In this portfolio I will present the projects that I made during my time at CODERS.BAY Vienna.

## Fullstack Projects

# Java/Web
- [Web administration tool](https://gitlab.com/brtq/fullstack.project)

An administration tool, what more is there to say? Simple, easy to use and easy on the eye.

# Java
- [PlasticSlug](https://gitlab.com/codersbay-fia-3/hello-world-2d-game)

Who doesn't love a good ol' 2D shooter? That's right, everyone likes to play something more simple, more relaxed,
no ranks, no pressure, just some good fun alone or with your friends. Made to be simple, meant to be played on every
desktop device, and is just here so you can relax and have some fun.

# Kotlin
- [FWB](https://gitlab.com/brtq/fwb)

No bull****, no lies, everyone knows that looks matter, especially on a dating app. This app focuses on looks and
contrast between different people. You are shown four potential matches instead of one and you can only choose one.
If you match you only have 48 hours to start a chat and there cannot be a longer break between messages than so you
have to think fast, this is to prevent one of the problems of dating apps: ghosting. If you exchange contacts your 
chats will be blocked for a short period so you can focus only on that one person, because this app is here so you
can find that one person that suits you.
